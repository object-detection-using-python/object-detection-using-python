## Topic
Object-Detection-Using-Python

## Description
We are creating an Object detection model using neural network-based transfer learning models which will detect the objects from the given images or videos or webcams.

## Installation
To run this model, here are the following steps to be followed:
1) Go to this link: https://github.com/pjreddie/darknet/blob/master/data/coco.names and copy all the labelsto store it inside a txt file in the directory where all the model components will be kept.
2) Then, go to this link: https://github.com/opencv/opencv/wiki/TensorFlow-Object-Detection-API and download the components of MobileNet-SSD v3 as we are going to use this transfer learning model.
3) After this, in the working directory bring the model's pbtxt file and frozen inference graph which is of pb format.
4) Then, to run code use the ipynb files in the order mentioned below:
Firstly, run the code given in Object_detection_1. Then, run the object_detection_in_video and then run CameraDetection ipynb file. 

## Authors and acknowledgment
Completing this project would not be possible without my team members whose contribution makes this project easier.
